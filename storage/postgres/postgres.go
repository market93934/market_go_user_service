package postgres

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market93934/market_go_user_service/config"
	"gitlab.com/market93934/market_go_user_service/storage"
)

type Store struct {
	db       *pgxpool.Pool
	staff    storage.StaffRepoI
	market   storage.MarketRepoI
	branch   storage.BranchRepoI
	provider storage.ProviderRepoI
}

func NewPostgres(ctx context.Context, cfg config.Config) (storage.StorageI, error) {

	config, err := pgxpool.ParseConfig(
		fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=disable",

			cfg.PostgresUser,
			cfg.PostgresPassword,
			cfg.PostgresHost,
			cfg.PostgresPort,
			cfg.PostgresDatabase,
		),
	)

	if err != nil {
		return nil, err
	}

	config.MaxConns = cfg.PostgresMaxConnections
	pool, err := pgxpool.ConnectConfig(ctx, config)
	if err != nil {
		return nil, err
	}

	return &Store{
		db: pool,
	}, err

}
func (s *Store) CloseDB() {
	s.db.Close()
}

func (s *Store) Staff() storage.StaffRepoI {
	if s.staff == nil {
		s.staff = NewStaffRepo(s.db)
	}
	return s.staff
}
func (s *Store) Market() storage.MarketRepoI {
	if s.market == nil {
		s.market = NewMarketRepo(s.db)
	}
	return s.market
}

func (s *Store) Branch() storage.BranchRepoI {
	if s.branch == nil {
		s.branch = NewBranchRepo(s.db)
	}
	return s.branch
}
func (s *Store) Provider() storage.ProviderRepoI {
	if s.provider == nil {
		s.provider = NewProviderRepo(s.db)
	}
	return s.provider
}
