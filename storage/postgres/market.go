package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market93934/market_go_user_service/genproto/user_service"
	"gitlab.com/market93934/market_go_user_service/pkg/helper"
)

type marketRepo struct {
	db *pgxpool.Pool
}

func NewMarketRepo(db *pgxpool.Pool) *marketRepo {
	return &marketRepo{
		db: db,
	}
}

func (c *marketRepo) Create(ctx context.Context, req *user_service.CreateMarket) (resp *user_service.MarketPrimaryKey, err error) {
	var id = uuid.New()
	query := `INSERT INTO "market" (
			id  ,
			name ,
			branch_id
			) VALUES ($1, $2, $3)
	`
	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.Name,
		req.BranchId,
	)
	if err != nil {
		return nil, err
	}

	return &user_service.MarketPrimaryKey{Id: id.String()}, nil

}

func (c *marketRepo) GetByPKey(ctx context.Context, req *user_service.MarketPrimaryKey) (resp *user_service.Market, err error) {
	query := `
		SELECT
			id,
			name ,
			branch_id,
			created_at,
			updated_at
		FROM "market"
		WHERE id = $1
	`

	var (
		id        sql.NullString
		name      sql.NullString
		branch_id sql.NullString
		createdAt sql.NullString
		updatedAt sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&branch_id,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &user_service.Market{
		Id:        id.String,
		Name:      name.String,
		BranchId:  branch_id.String,
		CreatedAt: createdAt.String,
		UpdatedAt: updatedAt.String,
	}

	return
}

func (c *marketRepo) GetAll(ctx context.Context, req *user_service.GetListMarketRequest) (resp *user_service.GetListMarketResponse, err error) {

	resp = &user_service.GetListMarketResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name ,
			branch_id,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "market"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}
	if req.SearchName != "" {
		filter += ` AND name ILIKE '%' ||'` + req.SearchName + `' || '%'`
	}
	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id        sql.NullString
			name      sql.NullString
			branch_id sql.NullString
			createdAt sql.NullString
			updatedAt sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&branch_id,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Markets = append(resp.Markets, &user_service.Market{
			Id:        id.String,
			Name:      name.String,
			BranchId:  branch_id.String,
			CreatedAt: createdAt.String,
			UpdatedAt: updatedAt.String,
		})
	}

	return
}

func (c *marketRepo) Update(ctx context.Context, req *user_service.UpdateMarket) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "market"
			SET
				name = :name,
				branch_id = :branch_id,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":        req.GetId(),
		"name":      req.GetName(),
		"branch_id": req.GetBranchId(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *marketRepo) Delete(ctx context.Context, req *user_service.MarketPrimaryKey) (*user_service.MarketEmpty, error) {

	query := `DELETE FROM "market" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return nil, err
	}

	return &user_service.MarketEmpty{}, nil
}
