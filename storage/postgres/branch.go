package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market93934/market_go_user_service/genproto/user_service"
	"gitlab.com/market93934/market_go_user_service/pkg/helper"
)

type branchRepo struct {
	db *pgxpool.Pool
}

func NewBranchRepo(db *pgxpool.Pool) *branchRepo {
	return &branchRepo{
		db: db,
	}
}

func (c *branchRepo) Create(ctx context.Context, req *user_service.CreateBranch) (resp *user_service.BranchPrimaryKey, err error) {
	var id = uuid.New()
	query := `INSERT INTO "branch" (
			id,
			name,
			branch_code,
			address,
			phone_number
			) VALUES ($1, $2, $3, $4, $5)
	`
	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.Name,
		req.BranchCode,
		req.Address,
		req.PhoneNumber,
	)
	if err != nil {
		return nil, err
	}

	return &user_service.BranchPrimaryKey{Id: id.String()}, nil

}

func (c *branchRepo) GetByPKey(ctx context.Context, req *user_service.BranchPrimaryKey) (resp *user_service.Branch, err error) {
	query := `
		SELECT
			id,
			name ,
			branch_code ,
			address,
			phone_number ,
			created_at,
			updated_at
		FROM "branch"
		WHERE id = $1
	`

	var (
		id           sql.NullString
		name         sql.NullString
		branch_code  sql.NullString
		address      sql.NullString
		phone_number sql.NullString
		createdAt    sql.NullString
		updatedAt    sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&branch_code,
		&address,
		&phone_number,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &user_service.Branch{
		Id:          id.String,
		Name:        name.String,
		BranchCode:  branch_code.String,
		Address:     address.String,
		PhoneNumber: phone_number.String,
		CreatedAt:   createdAt.String,
		UpdatedAt:   updatedAt.String,
	}

	return
}

func (c *branchRepo) GetAll(ctx context.Context, req *user_service.GetListBranchRequest) (resp *user_service.GetListBranchResponse, err error) {

	resp = &user_service.GetListBranchResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name ,
			branch_code ,
			address,
			phone_number ,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "branch"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}
	if req.SearchName != "" {
		filter += ` AND name ILIKE '%' || '` + req.SearchName + `' || '%'`
	}
	if req.SearchPhoneNumber != "" {
		filter += ` AND phone_number ILIKE '%' || '` + req.SearchPhoneNumber + `' || '%'`
	}
	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id           sql.NullString
			name         sql.NullString
			branch_code  sql.NullString
			address      sql.NullString
			phone_number sql.NullString
			createdAt    sql.NullString
			updatedAt    sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&branch_code,
			&address,
			&phone_number,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Branchs = append(resp.Branchs, &user_service.Branch{
			Id:          id.String,
			Name:        name.String,
			BranchCode:  branch_code.String,
			Address:     address.String,
			PhoneNumber: phone_number.String,
			CreatedAt:   createdAt.String,
			UpdatedAt:   updatedAt.String,
		})
	}

	return
}

func (c *branchRepo) Update(ctx context.Context, req *user_service.UpdateBranch) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "branch"
			SET
				name = :name,
				branch_code = :branch_code,
				address = :address,
				phone_number = :phone_number,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":           req.GetId(),
		"name":         req.GetName(),
		"branch_code":  req.GetBranchCode(),
		"address":      req.GetAddress(),
		"phone_number": req.GetPhoneNumber(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *branchRepo) Delete(ctx context.Context, req *user_service.BranchPrimaryKey) (*user_service.BranchEmpty, error) {

	query := `DELETE FROM "branch" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return nil, err
	}

	return &user_service.BranchEmpty{}, nil
}
