package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market93934/market_go_user_service/genproto/user_service"
	"gitlab.com/market93934/market_go_user_service/pkg/helper"
	"golang.org/x/crypto/bcrypt"
)

type staffRepo struct {
	db *pgxpool.Pool
}

func NewStaffRepo(db *pgxpool.Pool) *staffRepo {
	return &staffRepo{
		db: db,
	}
}

func (c *staffRepo) Create(ctx context.Context, req *user_service.CreateStaff) (resp *user_service.StaffPrimaryKey, err error) {
	var id = uuid.New()
	query := `INSERT INTO "staff" (
			id  ,
			name ,
			surname ,
			phone_number,
			login ,
			password ,
			user_type ,
			market_id
			) VALUES ($1, $2, $3, $4, $5, $6, $7, $8)
	`

	bytes, err := bcrypt.GenerateFromPassword([]byte(req.Password), 14)
	if err != nil {
		return nil, err
	}
	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.Name,
		req.Surname,
		req.PhoneNumber,
		req.Login,
		string(bytes),
		req.UserType,
		req.MarketId,
	)
	if err != nil {
		return nil, err
	}

	return &user_service.StaffPrimaryKey{Id: id.String()}, nil

}

func (c *staffRepo) GetByPKey(ctx context.Context, req *user_service.StaffPrimaryKey) (resp *user_service.Staff, err error) {
	var whereField = "id"
	if len(req.Login) > 0 {
		whereField = "login"
		req.Id = req.Login
	}
	query := `
		SELECT
			id,
			name ,
			surname ,
			phone_number,
			login ,
			password ,
			user_type ,
			market_id,
			created_at,
			updated_at
		FROM "staff"
		WHERE ` + whereField + ` = $1
	`

	var (
		id           sql.NullString
		name         sql.NullString
		surname      sql.NullString
		phone_number sql.NullString
		login        sql.NullString
		password     sql.NullString
		user_type    sql.NullString
		market_id    sql.NullString
		createdAt    sql.NullString
		updatedAt    sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&surname,
		&phone_number,
		&login,
		&password,
		&user_type,
		&market_id,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &user_service.Staff{
		Id:          id.String,
		Name:        name.String,
		Surname:     surname.String,
		PhoneNumber: phone_number.String,
		Login:       login.String,
		Password:    password.String,
		UserType:    user_type.String,
		MarketId:    market_id.String,
		CreatedAt:   createdAt.String,
		UpdatedAt:   updatedAt.String,
	}

	return
}

func (c *staffRepo) GetAll(ctx context.Context, req *user_service.GetListStaffRequest) (resp *user_service.GetListStaffResponse, err error) {

	resp = &user_service.GetListStaffResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name ,
			surname ,
			login ,
			password ,
			user_type ,
			market_id,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "staff"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}
	if req.SearchName != "" {
		filter += ` AND name ILIKE '%' ||'` + req.SearchName + `'|| '%'`
	}
	if req.SearchLastName != "" {
		filter += ` AND last_name ILIKE '%' ||'` + req.SearchLastName + `'|| '%'`
	}
	if req.SearchPhoneNumber != "" {
		filter += ` AND phone_number ILIKE '%' ||'` + req.SearchPhoneNumber + `'|| '%'`
	}
	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id        sql.NullString
			name      sql.NullString
			surname   sql.NullString
			login     sql.NullString
			password  sql.NullString
			user_type sql.NullString
			market_id sql.NullString
			createdAt sql.NullString
			updatedAt sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&surname,
			&login,
			&password,
			&user_type,
			&market_id,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Staffs = append(resp.Staffs, &user_service.Staff{
			Id:        id.String,
			Name:      name.String,
			Surname:   surname.String,
			Login:     login.String,
			Password:  password.String,
			UserType:  user_type.String,
			MarketId:  market_id.String,
			CreatedAt: createdAt.String,
			UpdatedAt: updatedAt.String,
		})
	}

	return
}

func (c *staffRepo) Update(ctx context.Context, req *user_service.UpdateStaff) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "staff"
			SET
				name = :name,
				surname = :surname,
				login = :login,
				password = :password,
				user_type = :user_type,
				market_id = :market_id,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":        req.GetId(),
		"name":      req.GetName(),
		"surname":   req.GetSurname(),
		"login":     req.GetLogin(),
		"password":  req.GetPassword(),
		"user_type": req.GetUserType(),
		"market_id": req.GetMarketId(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *staffRepo) Delete(ctx context.Context, req *user_service.StaffPrimaryKey) (*user_service.StaffEmpty, error) {

	query := `DELETE FROM "staff" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return nil, err
	}

	return &user_service.StaffEmpty{}, nil
}
