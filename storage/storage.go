package storage

import (
	"context"

	"gitlab.com/market93934/market_go_user_service/genproto/user_service"
)

type StorageI interface {
	CloseDB()
	Staff() StaffRepoI
	Market() MarketRepoI
	Branch() BranchRepoI
	Provider() ProviderRepoI
}
type StaffRepoI interface {
	Create(context.Context, *user_service.CreateStaff) (*user_service.StaffPrimaryKey, error)
	GetByPKey(context.Context, *user_service.StaffPrimaryKey) (*user_service.Staff, error)
	GetAll(context.Context, *user_service.GetListStaffRequest) (*user_service.GetListStaffResponse, error)
	Update(context.Context, *user_service.UpdateStaff) (int64, error)
	Delete(context.Context, *user_service.StaffPrimaryKey) (*user_service.StaffEmpty, error)
}
type MarketRepoI interface {
	Create(context.Context, *user_service.CreateMarket) (*user_service.MarketPrimaryKey, error)
	GetByPKey(context.Context, *user_service.MarketPrimaryKey) (*user_service.Market, error)
	GetAll(context.Context, *user_service.GetListMarketRequest) (*user_service.GetListMarketResponse, error)
	Update(context.Context, *user_service.UpdateMarket) (int64, error)
	Delete(context.Context, *user_service.MarketPrimaryKey) (*user_service.MarketEmpty, error)
}

type BranchRepoI interface {
	Create(context.Context, *user_service.CreateBranch) (*user_service.BranchPrimaryKey, error)
	GetByPKey(context.Context, *user_service.BranchPrimaryKey) (*user_service.Branch, error)
	GetAll(context.Context, *user_service.GetListBranchRequest) (*user_service.GetListBranchResponse, error)
	Update(context.Context, *user_service.UpdateBranch) (int64, error)
	Delete(context.Context, *user_service.BranchPrimaryKey) (*user_service.BranchEmpty, error)
}
type ProviderRepoI interface {
	Create(context.Context, *user_service.CreateProvider) (*user_service.ProviderPrimaryKey, error)
	GetByPKey(context.Context, *user_service.ProviderPrimaryKey) (*user_service.Provider, error)
	GetAll(context.Context, *user_service.GetListProviderRequest) (*user_service.GetListProviderResponse, error)
	Update(context.Context, *user_service.UpdateProvider) (int64, error)
	Delete(context.Context, *user_service.ProviderPrimaryKey) (*user_service.ProviderEmpty, error)
}
