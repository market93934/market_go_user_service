CREATE TABLE "branch" (
  "id" UUID PRIMARY KEY ,
  "name" varchar,
  "branch_code" varchar NOT NULL,
  "address" varchar,
  "phone_number" varchar,
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" timestamp
);


CREATE TABLE "market" (
  "id" UUID PRIMARY KEY ,
  "name" varchar,
  "branch_id" UUID ,
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" timestamp
);


CREATE TABLE "staff" (
  "id" UUID PRIMARY KEY ,
  "name" varchar,
  "surname" varchar,
  "phone_number" varchar,
  "login" varchar,
  "password" varchar,
  "user_type" varchar,
  "market_id" UUID ,
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" timestamp
);


CREATE TABLE "provider" (
  "id" UUID PRIMARY KEY ,
  "name" varchar,
  "phone_number" varchar,
  "active" boolean default true,
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" timestamp
);


