package grpc

import (
	"gitlab.com/market93934/market_go_user_service/config"
	"gitlab.com/market93934/market_go_user_service/genproto/user_service"
	"gitlab.com/market93934/market_go_user_service/grpc/client"
	"gitlab.com/market93934/market_go_user_service/grpc/service"
	"gitlab.com/market93934/market_go_user_service/pkg/logger"
	"gitlab.com/market93934/market_go_user_service/storage"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()
	user_service.RegisterStaffServiceServer(grpcServer, service.NewStaffService(cfg, log, strg, srvc))
	user_service.RegisterMarketServiceServer(grpcServer, service.NewMarketService(cfg, log, strg, srvc))
	user_service.RegisterBranchServiceServer(grpcServer, service.NewBranchService(cfg, log, strg, srvc))
	user_service.RegisterProviderServiceServer(grpcServer, service.NewProviderService(cfg, log, strg, srvc))
	reflection.Register(grpcServer)
	return
}
