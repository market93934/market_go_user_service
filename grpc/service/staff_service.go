package service

import (
	"context"

	"gitlab.com/market93934/market_go_user_service/config"
	"gitlab.com/market93934/market_go_user_service/genproto/user_service"
	"gitlab.com/market93934/market_go_user_service/grpc/client"
	"gitlab.com/market93934/market_go_user_service/pkg/logger"
	"gitlab.com/market93934/market_go_user_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type StaffService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*user_service.UnimplementedStaffServiceServer
}

func NewStaffService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *StaffService {
	return &StaffService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *StaffService) Create(ctx context.Context, req *user_service.CreateStaff) (resp *user_service.Staff, err error) {

	i.log.Info("---CreateStaff------>", logger.Any("req", req))

	pKey, err := i.strg.Staff().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateStaff->Staff->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Staff().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyStaff->Staff->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return
}
func (i *StaffService) GetByID(ctx context.Context, req *user_service.StaffPrimaryKey) (resp *user_service.Staff, err error) {

	i.log.Info("---GetStaffByID------>", logger.Any("req", req))

	resp, err = i.strg.Staff().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetStaffByID->Staff->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *StaffService) GetList(ctx context.Context, req *user_service.GetListStaffRequest) (*user_service.GetListStaffResponse, error) {
	i.log.Info("-------GetListduser-------", logger.Any("req", req))

	resp, err := i.strg.Staff().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetListStaff->Staff->Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return resp, nil
}

func (i *StaffService) Update(ctx context.Context, req *user_service.UpdateStaff) (resp *user_service.Staff, err error) {
	i.log.Info("---UpdateStaff------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Staff().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateStaff--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Staff().GetByPKey(ctx, &user_service.StaffPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetStaff->Staff->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *StaffService) Delete(ctx context.Context, req *user_service.StaffPrimaryKey) (resp *user_service.StaffEmpty, err error) {

	i.log.Info("---DeleteStaff------>", logger.Any("req", req))

	resp, err = i.strg.Staff().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteStaff->Staff->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}
