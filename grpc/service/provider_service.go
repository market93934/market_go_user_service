package service

import (
	"context"

	"gitlab.com/market93934/market_go_user_service/config"
	"gitlab.com/market93934/market_go_user_service/genproto/user_service"
	"gitlab.com/market93934/market_go_user_service/grpc/client"
	"gitlab.com/market93934/market_go_user_service/pkg/logger"
	"gitlab.com/market93934/market_go_user_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ProviderService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*user_service.UnimplementedProviderServiceServer
}

func NewProviderService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *ProviderService {
	return &ProviderService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *ProviderService) Create(ctx context.Context, req *user_service.CreateProvider) (resp *user_service.Provider, err error) {

	i.log.Info("---CreateProvider------>", logger.Any("req", req))

	pKey, err := i.strg.Provider().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateProvider->Provider->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Provider().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyProvider->Provider->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return
}
func (i *ProviderService) GetByID(ctx context.Context, req *user_service.ProviderPrimaryKey) (resp *user_service.Provider, err error) {

	i.log.Info("---GetProviderByID------>", logger.Any("req", req))

	resp, err = i.strg.Provider().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetProviderByID->Provider->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ProviderService) GetList(ctx context.Context, req *user_service.GetListProviderRequest) (*user_service.GetListProviderResponse, error) {
	i.log.Info("-------GetListduser-------", logger.Any("req", req))

	resp, err := i.strg.Provider().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetListProvider->Provider->Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return resp, nil
}

func (i *ProviderService) Update(ctx context.Context, req *user_service.UpdateProvider) (resp *user_service.Provider, err error) {
	i.log.Info("---UpdateProvider------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Provider().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateProvider--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Provider().GetByPKey(ctx, &user_service.ProviderPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetProvider->Provider->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *ProviderService) Delete(ctx context.Context, req *user_service.ProviderPrimaryKey) (resp *user_service.ProviderEmpty, err error) {

	i.log.Info("---DeleteProvider------>", logger.Any("req", req))

	resp, err = i.strg.Provider().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteProvider->Provider->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}
