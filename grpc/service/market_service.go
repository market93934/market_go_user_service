package service

import (
	"context"

	"gitlab.com/market93934/market_go_user_service/config"
	"gitlab.com/market93934/market_go_user_service/genproto/user_service"
	"gitlab.com/market93934/market_go_user_service/grpc/client"
	"gitlab.com/market93934/market_go_user_service/pkg/logger"
	"gitlab.com/market93934/market_go_user_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type MarketService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*user_service.UnimplementedMarketServiceServer
}

func NewMarketService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *MarketService {
	return &MarketService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *MarketService) Create(ctx context.Context, req *user_service.CreateMarket) (resp *user_service.Market, err error) {

	i.log.Info("---CreateMarket------>", logger.Any("req", req))

	pKey, err := i.strg.Market().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateMarket->Market->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Market().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyMarket->Market->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return
}
func (i *MarketService) GetByID(ctx context.Context, req *user_service.MarketPrimaryKey) (resp *user_service.Market, err error) {

	i.log.Info("---GetMarketByID------>", logger.Any("req", req))

	resp, err = i.strg.Market().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetMarketByID->Market->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *MarketService) GetList(ctx context.Context, req *user_service.GetListMarketRequest) (*user_service.GetListMarketResponse, error) {
	i.log.Info("-------GetListduser-------", logger.Any("req", req))

	resp, err := i.strg.Market().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetListMarket->Market->Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return resp, nil
}

func (i *MarketService) Update(ctx context.Context, req *user_service.UpdateMarket) (resp *user_service.Market, err error) {
	i.log.Info("---UpdateMarket------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Market().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateMarket--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Market().GetByPKey(ctx, &user_service.MarketPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetMarket->Market->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *MarketService) Delete(ctx context.Context, req *user_service.MarketPrimaryKey) (resp *user_service.MarketEmpty, err error) {

	i.log.Info("---DeleteMarket------>", logger.Any("req", req))

	resp, err = i.strg.Market().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteMarket->Market->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}
